#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
Use this profile to instantiate an experiment using Open Air Interface
to realize an end-to-end LTE mobile network. The profile provides
an OTS UE (Nexus 5) connected to an SDR-based eNodeB via a
controlled RF attenuator and connected to an EPC.  It also adds
a secondary listener node with GNU Radio available.

The profile includes the following resources:

  * Off-the-shelf Nexus 5 UE running Android 4.4.4 KitKat ('rue1')
  * SDR eNodeB (Intel NUC + USRP B210) running OAI eNodeB ('enb1')
  * A d430 compute node running the OAI EPC (HSS, MME, SPGW) ('epc')
  * A d430 compute node providing out-of-band ADB access to the UE ('adb-tgt')
  * A listener node (Intel NUC + USRP B210) with Ubuntu 18 and GNU Radio.

Startup scripts automatically configure OAI for the specific allocated resources.

For more detailed information:

  * [Getting Started](https://gitlab.flux.utah.edu/powder-profiles/OAI-GENERAL/blob/master/README.md)

""";

tourInstructions = """
After your experiment swapped in succesfully (i.e., is in the Ready state):

Log onto the `enb1` node and start the eNodeB service:

	sudo /local/repository/bin/enb.start.sh
	
To view the output of the eNodeB:

	sudo screen -r enb


Log onto the `epc` node and start the EPC services:

	sudo /local/repository/bin/start_oai.pl
	
To log onto the UE (`rue1`), first log onto the `adb-tgt` node and start up the adb daemon:

	pnadb -a

Then (still on `adb-tgt`) get an ADB shell on the UE by running:

	adb shell
	
If the UE successfully connected you should be able to ping an address on
the Internet from the ADB shell, e.g.,

	ping 8.8.8.8
	
If the UE did not connect by itself, (i.e., you get a "Network is unreachable" error),
you might have to reboot the UE (by executing `adb reboot` from the `adb-tgt` node,
or by executing `reboot` directly in the ADB shell on the UE). And then repeating
the `pnadb -a` and `adb shell` commands to get back on the UE to test.


While OAI is still a system in development and may be unstable, you can usually recover
from any issue by running `start_oai.pl` to restart all the services.

  * [More details](https://gitlab.flux.utah.edu/powder-profiles/OAI-GENERAL/blob/master/README.md)

""";


#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_DS = "urn:publicid:IDN+emulab.net:phantomnet+ltdataset+oai-develop"
    OAI_SIM_DS = "urn:publicid:IDN+emulab.net:phantomnet+dataset+PhantomNet:oai"
    UE_IMG  = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    OAI_EPC_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU16-64-OAIEPC")
    OAI_ENB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:OAI-Real-Hardware.enb1")
    GR_IMAGE = 'urn:publicid:IDN+emulab.net+image+PowderTeam:UBUNTU18-GR-PREREQS'
    GR_DATASET = 'urn:publicid:IDN+emulab.net:powderteam+imdataset+gr-rfnoc'
    OAI_CONF_SCRIPT = "/usr/bin/sudo /local/repository/bin/config_oai.pl"
    NUC_HWTYPE = "nuc5300"
    UE_HWTYPE = "nexus5"

def connectOAI_DS(node, sim):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if sim == 1:
	bs.dataset = GLOBALS.OAI_SIM_DS
    else:
	bs.dataset = GLOBALS.OAI_DS
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
pc.defineParameter("FIXED_UE", "Bind to a specific Nexus 5",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF Nexus 5 UE to allocate (e.g., 'ue1').  Leave blank to let the mapping algorithm choose.")
pc.defineParameter("FIXED_ENB", "Bind to a specific NUC+B210",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF NUC+B210 device to allocate (e.g., 'nuc1').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.")

pc.defineParameter("FIXED_LISTENER", "Bind to a specific NUC+B210",
                   portal.ParameterType.STRING, "", advanced=True,
                   longDescription="Input the name of a POWDER controlled RF NUC+B210 device to allocate (e.g., 'nuc2').  Leave blank to let the mapping algorithm choose.  If you bind both Nexus 5 (UE) and listener devices, mapping will fail unless there is path between them via the attenuator matrix.")

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()
epclink = request.Link("s1-lan")

# Checking for oaisim

# Add a node to act as the ADB target host
adb_t = request.RawPC("adb-tgt")
adb_t.disk_image = GLOBALS.ADB_IMG

# Add a NUC eNB node.
enb1 = request.RawPC("enb1")
if params.FIXED_ENB:
    enb1.component_id = params.FIXED_ENB
enb1.hardware_type = GLOBALS.NUC_HWTYPE
enb1.disk_image = GLOBALS.OAI_ENB_IMG
enb1.Desire( "rf-controlled", 1 )
connectOAI_DS(enb1, 0)
enb1.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r ENB"))
enb1_rue1_rf = enb1.addInterface("rue1_rf")

# Add an OTS (Nexus 5) UE
rue1 = request.UE("rue1")
if params.FIXED_UE:
    rue1.component_id = params.FIXED_UE
rue1.hardware_type = GLOBALS.UE_HWTYPE
rue1.disk_image = GLOBALS.UE_IMG
rue1.Desire( "rf-controlled", 1 )    
rue1.adb_target = "adb-tgt"
rue1_enb1_rf = rue1.addInterface("enb1_rf")
rue1_listener_rf = rue1.addInterface("listener_rf")

# Add listener NUC+B210 (GNU Radio)
listener = request.RawPC("listener")
if params.FIXED_LISTENER:
    listener.component_id = params.FIXED_LISTENER
listener.hardware_type = GLOBALS.NUC_HWTYPE
listener.disk_image = GLOBALS.GR_IMAGE
lsnbs1 = listener.Blockstore("lsnbs1", "/opt")
lsnbs1.dataset = GLOBALS.GR_DATASET
listener.Desire( "rf-controlled", 1 )
listener_rue1_rf = listener.addInterface("rue1_rf")

# Create the RF link between the Nexus 5 UE and eNodeB
rflink1 = request.RFLink("rflink1")
rflink1.addInterface(enb1_rue1_rf)
rflink1.addInterface(rue1_enb1_rf)

# Create the RF link between the Nexus 5 UE and the listener
rflink2 = request.RFLink("rflink2")
rflink2.addInterface(listener_rue1_rf)
rflink2.addInterface(rue1_listener_rf)

# Add a link connecting the NUC eNB and the OAI EPC node.
epclink.addNode(enb1)

# Add OAI EPC (HSS, MME, SPGW) node.
epc = request.RawPC("epc")
epc.disk_image = GLOBALS.OAI_EPC_IMG
epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
connectOAI_DS(epc, 0)
 
epclink.addNode(epc)
epclink.link_multiplexing = True
epclink.vlan_tagging = True
epclink.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

#
# Print and go!
#
pc.printRequestRSpec(request)
